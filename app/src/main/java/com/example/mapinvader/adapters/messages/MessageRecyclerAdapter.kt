package com.example.mapinvader.adapters.messages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mapinvader.R
import com.example.mapinvader.models.Message

class MessageRecyclerAdapter(messages: MutableList<Message>) : RecyclerView.Adapter<MessageViewHolder>() {
    private val messages: MutableList<Message, > = mutableListOf()

    init {
        this.messages.addAll(messages)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val messagesView = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
        return MessageViewHolder(messagesView)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    fun dataAdded(message: MutableList<Message>) {
        val number = itemCount
        this.messages.clear()
        this.messages.addAll(message)
        if (itemCount == number) return
        notifyItemInserted(itemCount - 1)
    }
}