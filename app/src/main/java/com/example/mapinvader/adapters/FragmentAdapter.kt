package com.example.mapinvader.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.mapinvader.fragments.ChatFragment
import com.example.mapinvader.fragments.MapFragment

private const val NUM_TABS = 2

class FragmentAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    override fun getItemCount(): Int {
        return NUM_TABS
    }

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> return MapFragment.newInstance()
            1 -> return ChatFragment.newInstance()
        }
        return ChatFragment.newInstance()
    }
}