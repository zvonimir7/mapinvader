package com.example.mapinvader.adapters.messages

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mapinvader.R
import com.example.mapinvader.helpers.DataHolder
import com.example.mapinvader.helpers.millisToDate
import com.example.mapinvader.models.Message

class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(message: Message) {
        val msgMy = itemView.findViewById<TextView>(R.id.messageMyTv)
        val msgHis = itemView.findViewById<TextView>(R.id.messageTv)
        val msgHisSender = itemView.findViewById<TextView>(R.id.tv_itemChat_hisName)
        val msgMyTime = itemView.findViewById<TextView>(R.id.tv_chat_MyTime)
        val msgHisTime = itemView.findViewById<TextView>(R.id.tv_chat_HisTime)

        val timeToShow: String = millisToDate(message.timeSent)
        msgHisTime.text = timeToShow
        msgMyTime.text = timeToShow

        if (DataHolder.getLoggedUser() == message.senderId) {
            msgMy.text = message.textMessage
            msgHis.visibility = View.GONE
            msgMy.visibility = View.VISIBLE
            msgHisSender.visibility = View.GONE
        } else {
            msgHisSender.text = message.senderId
            msgHis.text = message.textMessage
            msgHis.visibility = View.VISIBLE
            msgMy.visibility = View.GONE
        }

        msgMy.setOnClickListener {
            if (msgMyTime.visibility == View.GONE) {
                msgMyTime.visibility = View.VISIBLE
            } else {
                msgMyTime.visibility = View.GONE
            }
        }

        msgHis.setOnClickListener {
            if (msgHisTime.visibility == View.GONE) {
                msgHisTime.visibility = View.VISIBLE
            } else {
                msgHisTime.visibility = View.GONE
            }
        }
    }
}