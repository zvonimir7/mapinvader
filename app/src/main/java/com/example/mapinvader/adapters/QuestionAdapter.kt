package com.example.mapinvader.adapters

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.mapinvader.data.Firebase
import com.example.mapinvader.helpers.MyDialogues
import com.example.mapinvader.models.Question

class QuestionAdapter(private val questions: MutableList<Question>, private val context: Context) {
    init {
        Firebase.fetchQuestions()
    }

    fun getRandomQuestion(gym: String) {
        if (questions.isEmpty()) {
            Toast.makeText(context, "Try later in few minutes, problem with You internet connection", Toast.LENGTH_SHORT).show()
            return
        }

        val question = this.questions.random()
        MyDialogues().showConquerDialog(context, question.question.toString(), gym)
    }
}