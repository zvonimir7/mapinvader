package com.example.mapinvader.adapters

import com.example.mapinvader.MapInvaderApplication
import com.example.mapinvader.R
import com.example.mapinvader.data.GymRepository
import com.example.mapinvader.helpers.DataHolder
import com.example.mapinvader.helpers.getBitmapFromVectorDrawable
import com.example.mapinvader.models.Gym
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil

class GymAdapter(private val googleMap: GoogleMap) {
    private var gyms = mutableListOf<Gym>()
    private val markers = mutableListOf<Marker>()
    private val circleAroundMarker = mutableListOf<Circle>()
    private val radiusAroundMarker: Double = 75.00

    fun dataAdded(gym: MutableList<Gym>) {
        val difference = gym.find { !gyms.contains(it) }
        if (difference != null) {
            gyms.add(difference)
        }

        if (difference != null) {
            createNewMarkers(difference)
        }
    }

    private fun createNewMarkers(difference: Gym) {
        val markerLatLng = LatLng(difference.latitude, difference.longitude)
        val markerIcon: Int = if (difference.owner == DataHolder.getLoggedUser()) {
            R.drawable.icon_point_ally
        } else {
            R.drawable.icon_point_enemy
        }

        val marker: Marker = googleMap.addMarker(MarkerOptions()
            .position(markerLatLng).title(difference.owner)
            .icon(getBitmapFromVectorDrawable(MapInvaderApplication.ApplicationContext, markerIcon))
        )

        markers.add(marker)
        createCircleAroundMarker(marker)
    }

    fun clearAllMarkers() {
        markers.forEach { it.remove() }
        circleAroundMarker.forEach { it.remove() }
        
        circleAroundMarker.clear()
        markers.clear()
        gyms.clear()
    }

    private fun createCircleAroundMarker(marker: Marker) {
        val circleOptions = CircleOptions()
            .center(LatLng(marker.position.latitude, marker.position.longitude))
            .radius(radiusAroundMarker)

        val circleMarker: Circle = googleMap.addCircle(circleOptions)
        circleAroundMarker.add(circleMarker)
    }

    fun getGymIdByPosition(position: LatLng?): String? {
        return GymRepository.gyms.find { it.latitude == position?.latitude && it.longitude == position.longitude }?.id
    }

    fun dataModified(gym: Gym) {
        val gymModified = gyms.find { it.longitude.equals(gym.longitude) && it.latitude.equals(gym.latitude) }

        val marker = markers.find { it.position == LatLng(gymModified!!.latitude, gymModified.longitude) }
        marker!!.title = gymModified!!.owner

        val markerIcon: Int = if (gymModified.owner == DataHolder.getLoggedUser()) {
            R.drawable.icon_point_ally
        } else {
            R.drawable.icon_point_enemy
        }

        marker.setIcon(getBitmapFromVectorDrawable(MapInvaderApplication.ApplicationContext, markerIcon))
    }

    fun checkForNearbyGyms(myPosition: LatLng): Marker? {
        return markers.find {
            SphericalUtil.computeDistanceBetween(myPosition, it.position) <= (radiusAroundMarker)
        }
    }

    fun areSameGyms(gym1: Marker, gym2: Marker?): Boolean {
        if (gym1.position == gym2?.position) return true
        return false
    }
}