package com.example.mapinvader.models

import java.util.*

class User(val id: String, val username: String, val password: String) {
    val search_name: String = username.lowercase()
}