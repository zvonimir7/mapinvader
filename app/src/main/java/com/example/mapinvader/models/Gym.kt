package com.example.mapinvader.models

class Gym(val id: String, var owner: String, val latitude: Double, val longitude: Double)