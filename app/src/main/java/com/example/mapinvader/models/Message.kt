package com.example.mapinvader.models

class Message(val textMessage: String, val senderId: String, val timeSent: Long)