package com.example.mapinvader.models

class Question(val question: String? = null, val positive: String? = null, val negative1: String? = null, val negative2: String? = null, val negative3: String? = null)