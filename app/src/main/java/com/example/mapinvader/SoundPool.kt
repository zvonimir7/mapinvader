package com.example.mapinvader

import android.media.SoundPool

class SoundPool {
    private lateinit var soundPool: SoundPool
    var soundMap: HashMap<Int, Int> = HashMap()

    fun loadSounds() {
        soundPool = SoundPool.Builder().setMaxStreams(1).build()
        soundMap[R.raw.game_bump] = soundPool.load(MapInvaderApplication.ApplicationContext, R.raw.game_bump, 1)
    }

    fun playSound(selectedSound: Int) {
        val soundID = soundMap[selectedSound] ?: 0
        soundPool.play(soundID, 1f, 1f, 1, 0, 1f)
    }
}