package com.example.mapinvader

import android.app.Application
import android.content.Context

class MapInvaderApplication : Application() {
    companion object {
        lateinit var ApplicationContext: Context
            private set
    }

    override fun onCreate() {
        super.onCreate()
        ApplicationContext = this
    }
}