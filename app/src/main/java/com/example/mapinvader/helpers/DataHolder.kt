package com.example.mapinvader.helpers

object DataHolder {
    private lateinit var currentUser: String

    fun getLoggedUser(): String {
        return this.currentUser
    }

    fun setLoggedUser(username: String) {
        this.currentUser = username
    }
}