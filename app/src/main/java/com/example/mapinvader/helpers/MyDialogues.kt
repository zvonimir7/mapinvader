package com.example.mapinvader.helpers

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import com.example.mapinvader.MapInvaderApplication
import com.example.mapinvader.data.localData.PreferenceManager
import com.example.mapinvader.ui.MainActivity
import com.example.mapinvader.ui.QuestionActivity

class MyDialogues {
    fun showRememberMeDialog(username: String, password: String, context: Context) {
        val alertDialog = AlertDialog.Builder(context).create()
        alertDialog.setTitle("Remember me?")
        alertDialog.setMessage("Do You want to be remembered? ;)")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes") { _, _ ->
            PreferenceManager().saveUserCredentials(username, password)
            startMainActivity(username)
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No") { _, _ ->
            PreferenceManager().saveUserCredentials("default", "default")
            startMainActivity(username)
        }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams
    }

    fun showConquerDialog(context: Context, question: String, gym: String) {
        val alertDialog = AlertDialog.Builder(context).create()
        alertDialog.setTitle("Conquer?")
        alertDialog.setMessage("Do You want to try to steal this gym ownership from a current owner?")

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes") { _, _ ->
            val intent = Intent(MapInvaderApplication.ApplicationContext, QuestionActivity::class.java)
            intent.putExtra("questionTitle", question)
            intent.putExtra("gymId", gym)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(MapInvaderApplication.ApplicationContext, intent, null)
        }

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No") { _, _ ->
            alertDialog.dismiss()
        }
        alertDialog.show()

        val btnPositive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        val btnNegative = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

        val layoutParams = btnPositive.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 10f
        btnPositive.layoutParams = layoutParams
        btnNegative.layoutParams = layoutParams
    }

    fun startMainActivity(username: String) {
        DataHolder.setLoggedUser(username)

        val intent = Intent(MapInvaderApplication.ApplicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(MapInvaderApplication.ApplicationContext, intent, null)
    }
}