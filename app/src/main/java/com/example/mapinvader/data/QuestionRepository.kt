package com.example.mapinvader.data

import com.example.mapinvader.models.Question

object QuestionRepository {
    val questions: MutableList<Question> = mutableListOf()

    fun addQuestion(question: Question) = questions.add(question)
    fun removeAllQuestions() = questions.clear()
}