package com.example.mapinvader.data

import com.example.mapinvader.models.User

object UserRepository {
    val users: MutableList<User> = mutableListOf()

    fun add(user: User) = users.add(user)
    fun clearAllUsers() = users.clear()
}