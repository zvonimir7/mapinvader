package com.example.mapinvader.data

import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.mapinvader.fragments.ChatFragment
import com.example.mapinvader.fragments.MapFragment
import com.example.mapinvader.helpers.MyDialogues
import com.example.mapinvader.models.Gym
import com.example.mapinvader.models.Message
import com.example.mapinvader.models.Question
import com.example.mapinvader.models.User
import com.google.firebase.database.*
import com.google.firebase.database.ktx.getValue

object Firebase {
    private val dbInstance = FirebaseDatabase.getInstance("https://map-invader-default-rtdb.europe-west1.firebasedatabase.app/")
    private val usersDbReference = dbInstance.getReference("Users")
    private val chatDbReference = dbInstance.getReference("Messages")
    private val gymsDbReference = dbInstance.getReference("Gyms")
    private val questionsDbReference = dbInstance.getReference("Questions")
    private val reportsDbReference = dbInstance.getReference("Reports")
    private var childEventListenerUsers: ChildEventListener? = null
    private var childEventListenerMessages: ChildEventListener? = null
    private var childEventListenerGyms: ChildEventListener? = null

    fun fetchUsers() {
        childEventListenerUsers = object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val userData = snapshot.value as HashMap<*, *>

                val user = User(userData["id"].toString(), userData["username"].toString(), userData["password"].toString())
                UserRepository.add(user)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onCancelled(error: DatabaseError) {}
        }

        val query: Query = usersDbReference.orderByChild("username")
        query.addChildEventListener(childEventListenerUsers as ChildEventListener)
    }

    fun fetchGyms() {
        childEventListenerGyms = object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val gymData = snapshot.value as HashMap<*, *>
                val gym = Gym(gymData["id"].toString(), gymData["owner"].toString(), gymData["latitude"].toString().toDouble(), gymData["longitude"].toString().toDouble())

                GymRepository.addGym(gym)
                MapFragment.gymAdapter.dataAdded(GymRepository.gyms)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val gymData = snapshot.value as HashMap<*, *>
                val gym = Gym(gymData["id"].toString(), gymData["owner"].toString(), gymData["latitude"].toString().toDouble(), gymData["longitude"].toString().toDouble())

                GymRepository.modifyGymOwner(gym)
                MapFragment.gymAdapter.dataModified(gym)
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onCancelled(error: DatabaseError) {}
        }

        val query: Query = gymsDbReference.orderByChild("id")
        query.addChildEventListener(childEventListenerGyms as ChildEventListener)
    }

    //For testing purposes
    fun pushNewGymToDb(owner: String, lat: Double, long: Double) {
        /*val latlng = LatLng()
        Firebase.pushNewGymToDb("test", latlng.latitude, latlng.longitude)*/

        val id = gymsDbReference.push().key as String
        val gym = Gym(id, owner, lat, long)

        gymsDbReference.child(id).setValue(gym).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.e("gym to db done", gym.owner + " " + gym.latitude + " " + gym.longitude)
            } else {
                Log.e("gym to db failed", gym.owner + " " + gym.latitude + " " + gym.longitude)
            }
        }
    }

    //For testing purposes
    fun pushNewQuestionToDb(question: Question) {
        /*val questionTitle = ""
        val questionPositive = ""
        val questionNegative1 = ""
        val questionNegative2 = ""
        val questionNegative3 = ""
        val question = Question(questionTitle, questionPositive, questionNegative1, questionNegative2, questionNegative3)
        Firebase.pushNewQuestionToDb(question)*/

        questionsDbReference.push().setValue(question).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.e("question to db done", question.question + " " + question.positive)
            } else {
                Log.e("question to db failed", question.question + " " + question.positive)
            }
        }
    }

    fun unsubscribeGymListener() {
        GymRepository.removeAllGyms()
        gymsDbReference.removeEventListener(childEventListenerGyms as ChildEventListener)
    }

    fun fetchQuestions() {
        val questionListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val questions = dataSnapshot.children

                questions.forEach {
                    val question = it.getValue<Question>()
                    if (question != null) {
                        QuestionRepository.addQuestion(question)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        questionsDbReference.addListenerForSingleValueEvent(questionListener)
    }

    fun authenticateUser(username: String, password: String): Int {
        UserRepository.users.forEach {
            if (it.search_name == username.lowercase() && it.password != password) {
                return 2 //wrong password
            } else if (it.search_name == username.lowercase() && it.password == password) {
                return 1 //login successful
            }
        }

        return 3 //register newUser
    }

    fun registerNewUser(username: String, password: String, context: Context) {
        val id = usersDbReference.push().key as String
        val user = User(id, username, password)
        usersDbReference.child(id).setValue(user).addOnCompleteListener {
            if (it.isSuccessful) {
                MyDialogues().showRememberMeDialog(username, password, context)
            } else {
                Toast.makeText(context, it.exception?.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun changeOwnerOfGymId(gymId: String, newOwner: String, context: Activity) {
        gymsDbReference.child(gymId).child("owner").setValue(newOwner).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(context, "Gym successfully conquered!", Toast.LENGTH_LONG).show()
                context.finish()
            } else {
                Toast.makeText(context, it.exception?.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun unsubscribeUserListener() {
        UserRepository.clearAllUsers()
        usersDbReference.removeEventListener(childEventListenerUsers as ChildEventListener)
    }

    fun fetchMessages() {
        childEventListenerMessages = object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val messageData = snapshot.value as HashMap<*, *>
                val senderId = messageData["senderId"].toString()
                val timeSent = messageData["timeSent"].toString().toLong()
                val textMessage = messageData["textMessage"].toString()

                val message = Message(textMessage, senderId, timeSent)
                MessageRepository.addMessage(message)
                ChatFragment.adapter.dataAdded(MessageRepository.messages)
                ChatFragment.recyclerView.scrollToPosition(ChatFragment.adapter.itemCount - 1)
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildRemoved(snapshot: DataSnapshot) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onCancelled(error: DatabaseError) {}

        }

        val query: Query = chatDbReference.orderByChild("timeSent")
        query.addChildEventListener(childEventListenerMessages as ChildEventListener)
    }

    fun unsubscribeMessageListener() {
        MessageRepository.removeAllMessages()
        chatDbReference.removeEventListener(childEventListenerMessages as ChildEventListener)
    }

    fun registerNewMessage(message: Message) {
        chatDbReference.push().setValue(message).addOnCompleteListener {
            if (it.isSuccessful) {
                //message has been added to db
            }
        }
    }

    fun reportQuestionTitle(title: String, user: String) {
        reportsDbReference.child(title).child("Reported By").setValue(user).addOnCompleteListener {
            if (it.isSuccessful) {
                //report has been added to db
            }
        }
    }
}