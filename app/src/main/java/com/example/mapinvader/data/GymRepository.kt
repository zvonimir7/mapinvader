package com.example.mapinvader.data

import com.example.mapinvader.models.Gym

object GymRepository {
    val gyms: MutableList<Gym> = mutableListOf()

    fun addGym(gym: Gym) = gyms.add(gym)
    fun removeAllGyms() = gyms.clear()
    fun modifyGymOwner(gym: Gym) {
        gyms.find { it.latitude.equals(gym.latitude) && it.longitude.equals(gym.longitude) }?.owner = gym.owner
    }
}