package com.example.mapinvader.data

import com.example.mapinvader.models.Message

object MessageRepository {
    val messages: MutableList<Message> = mutableListOf()

    fun addMessage(message: Message) = messages.add(message)
    fun removeAllMessages() = messages.clear()
}