package com.example.mapinvader.data.localData

import android.content.Context
import com.example.mapinvader.MapInvaderApplication

class PreferenceManager {
    companion object {
        const val PREFS_FILE = "MapInvaderPreferences"
        const val PREFS_KEY_USERNAME = "username"
        const val PREFS_KEY_PASSWORD = "password"
    }

    fun saveUserCredentials(username: String, password: String) {
        val sharedPreferences = MapInvaderApplication.ApplicationContext.getSharedPreferences(
            PREFS_FILE,
            Context.MODE_PRIVATE
        )
        val editor = sharedPreferences.edit()
        editor.putString(PREFS_KEY_USERNAME, username)
        editor.putString(PREFS_KEY_PASSWORD, password)
        editor.apply()
    }

    fun retrieveUsername(): String {
        val sharedPreferences = MapInvaderApplication.ApplicationContext.getSharedPreferences(
            PREFS_FILE,
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString(PREFS_KEY_USERNAME, "default")!!
    }

    fun retrievePassword(): String {
        val sharedPreferences = MapInvaderApplication.ApplicationContext.getSharedPreferences(
            PREFS_FILE,
            Context.MODE_PRIVATE
        )
        return sharedPreferences.getString(PREFS_KEY_PASSWORD, "default")!!
    }
}