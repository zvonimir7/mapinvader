package com.example.mapinvader.ui

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mapinvader.data.Firebase
import com.example.mapinvader.data.localData.PreferenceManager
import com.example.mapinvader.databinding.ActivityStartBinding
import com.example.mapinvader.helpers.MyDialogues
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_INDEFINITE
import com.google.android.material.snackbar.Snackbar

class StartActivity : AppCompatActivity() {
    private lateinit var startBinding: ActivityStartBinding
    private var rememberMe: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startBinding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(startBinding.root)

        tryToLogin()
        initializeUI()
    }

    private fun tryToLogin() {
        val username: String = PreferenceManager().retrieveUsername()
        val password: String = PreferenceManager().retrievePassword()

        if (PreferenceManager().retrieveUsername() != "default") {
            startBinding.etAuthPassword.setText(password)
            startBinding.etAuthNickname.setText(username)

            rememberMe = true
        }
    }

    private fun initializeUI() {
        startBinding.btnAuthStart.setOnClickListener {
            val username = startBinding.etAuthNickname.text.toString().trim()
            val password = startBinding.etAuthPassword.text.toString().trim()

            if (username.isEmpty() || password.isEmpty()) {
                infoSnackBar("Username and password are obligatory!", "Gotcha!")
            } else if (username.length < 3 || username.length > 32) {
                infoSnackBar("Username has to be 3 or more characters long!", "Gotcha!")
            } else if (password.length < 6) {
                startBinding.etAuthPassword.text.clear()
                infoSnackBar("Password has to be 6 or more characters long!", "Gotcha!")
            } else if (username == "default") {
                infoSnackBar("We got a funny guy here! or a tester maybe? :/", "You got me!")
            } else {
                tryLoginThisUser(username, password)
            }
        }
    }

    private fun tryLoginThisUser(username: String, password: String) {
        if (!isNetworkAvailable(this)) {
            infoSnackBar("Check Your internet connection!", "Ok!")

            return
        }

        when (Firebase.authenticateUser(username, password)) {
            2 -> {
                rememberMe = false
                startBinding.etAuthPassword.text.clear()
                infoSnackBar("Password is incorrect!", "Gotcha!")
            }
            3 -> {
                val snack = Snackbar.make(startBinding.root,
                    "Ooh, a new user? Do You want to sign up?",
                    Snackbar.LENGTH_LONG)
                    .setAction("Yes!") {
                        Firebase.registerNewUser(username, password, this)
                    }
                snack.show()
            }
            1 -> {
                if (rememberMe) {
                    MyDialogues().startMainActivity(username)
                } else {
                    MyDialogues().showRememberMeDialog(username, password, this)
                }
            }

        }
    }

    private fun infoSnackBar(infoDesc: String, btnInfo: String) {
        val snack = Snackbar.make(startBinding.root, infoDesc, Snackbar.LENGTH_LONG)
            .setAction(btnInfo) { }.setDuration(LENGTH_INDEFINITE)
        snack.show()
    }

    override fun onResume() {
        super.onResume()
        Firebase.fetchUsers()
    }

    override fun onPause() {
        super.onPause()
        Firebase.unsubscribeUserListener()
    }

    @SuppressLint("MissingPermission")
    fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        return true
                    }
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo

            if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                return true
            }
        }

        return false
    }
}