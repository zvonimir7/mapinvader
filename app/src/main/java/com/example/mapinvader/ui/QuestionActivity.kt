package com.example.mapinvader.ui

import android.graphics.Color
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mapinvader.R
import com.example.mapinvader.data.Firebase
import com.example.mapinvader.data.QuestionRepository
import com.example.mapinvader.databinding.ActivityQuestionBinding
import com.example.mapinvader.helpers.DataHolder


class QuestionActivity : AppCompatActivity() {
    private lateinit var questionBinding: ActivityQuestionBinding
    private var isVoted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        questionBinding = ActivityQuestionBinding.inflate(layoutInflater)
        setContentView(questionBinding.root)

        initializeUI()
    }

    private fun initializeUI() {
        val listView = questionBinding.lvQuestionsAnswers
        val list: ArrayList<String> = ArrayList()
        val arrayAdapter = ArrayAdapter(this, R.layout.item_answer, R.id.tv_itemAnswers_answer, list)

        val questionData = QuestionRepository.questions.find { it.question == intent.getStringExtra("questionTitle").toString() }
        questionBinding.questionTitle.text = questionData?.question.toString()
        list.add(questionData?.negative1.toString())
        list.add(questionData?.negative2.toString())
        list.add(questionData?.negative3.toString())
        list.add(questionData?.positive.toString())
        list.shuffle()
        arrayAdapter.notifyDataSetChanged()
        listView.adapter = arrayAdapter
        listView.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        listView.setItemChecked(2, true)

        listView.setOnItemClickListener { adapterView, view, position, _ ->
            val element = listView.adapter.getItem(position)
            if (isVoted) return@setOnItemClickListener
            isVoted = true

            if (questionData?.positive.toString() == element) {
                listView.adapter.getView(position, view, adapterView).setBackgroundColor(Color.rgb(0, 255, 0))

                Firebase.changeOwnerOfGymId(intent.getStringExtra("gymId").toString(), DataHolder.getLoggedUser(), this)
            } else {
                listView.adapter.getView(position, view, adapterView).setBackgroundColor(Color.rgb(255, 0, 0))
                adapterView.getChildAt(list.indexOf(questionData?.positive)).setBackgroundColor(Color.rgb(0, 255, 0))
            }
        }

        questionBinding.btnQuestionsSkip.setOnClickListener { finish() }
        questionBinding.btnQuestionsReport.setOnClickListener {
            Firebase.reportQuestionTitle(questionData?.question.toString(), DataHolder.getLoggedUser())
            Toast.makeText(this, "Thank You for Your report! Question will be investigated.", Toast.LENGTH_SHORT).show()
        }
    }
}