package com.example.mapinvader.ui

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.mapinvader.R
import com.example.mapinvader.adapters.FragmentAdapter
import com.example.mapinvader.adapters.messages.MessageRecyclerAdapter
import com.example.mapinvader.data.localData.PreferenceManager
import com.example.mapinvader.databinding.ActivityMainBinding
import com.example.mapinvader.fragments.ChatFragment
import com.example.mapinvader.helpers.DataHolder
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    private var adapter = FragmentAdapter(supportFragmentManager, lifecycle)

    companion object {
        lateinit var viewPager: ViewPager2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        initializeUI()
    }

    private fun initializeUI() {
        val fragmentTitles = arrayOf("Map", "Chat")

        viewPager = mainBinding.viewPager
        val tabLayout = mainBinding.tabLayout
        viewPager.adapter = adapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = fragmentTitles[position]
        }.attach()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        if (item.itemId == R.id.LogoutOption) {
            PreferenceManager().saveUserCredentials("default", "default")

            val logoutIntent = Intent(this, StartActivity::class.java)
            startActivity(logoutIntent)
            Toast.makeText(this, "You have successfully logged out", Toast.LENGTH_SHORT).show()
            finish()
        } else if (item.itemId == R.id.ProfileOption) {
            val profileIntent = Intent(this, ProfileActivity::class.java)
            startActivity(profileIntent)
        }
        return true
    }
}