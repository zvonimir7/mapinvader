package com.example.mapinvader.ui

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.mapinvader.databinding.ActivityProfileBinding
import com.example.mapinvader.helpers.DataHolder
import com.example.mapinvader.models.Gym
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileActivity : AppCompatActivity() {
    private lateinit var profileBinding: ActivityProfileBinding

    private val dbInstance = FirebaseDatabase.getInstance("https://map-invader-default-rtdb.europe-west1.firebasedatabase.app/")
    private val gymsDbReference = dbInstance.getReference("Gyms")

    private var countOwner: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileBinding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(profileBinding.root)

        initializeUI()
    }

    private fun initializeUI() {
        profileBinding.tvProfileName.text = DataHolder.getLoggedUser()

        fetchGymCounterForUser(DataHolder.getLoggedUser())
    }

    private fun fetchGymCounterForUser(gymOwner: String) {
        val gymCountListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val gyms = dataSnapshot.children

                gyms.forEach {
                    val gymData = it.value as HashMap<*, *>
                    val gym = Gym(gymData["id"].toString(), gymData["owner"].toString(), gymData["latitude"].toString().toDouble(), gymData["longitude"].toString().toDouble())

                    if (gym.owner == gymOwner) {
                        countOwner++
                        profileBinding.tvProfileCount.text = countOwner.toString()
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(ContentValues.TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        gymsDbReference.addListenerForSingleValueEvent(gymCountListener)
    }
}