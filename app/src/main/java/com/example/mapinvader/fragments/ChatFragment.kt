package com.example.mapinvader.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mapinvader.adapters.messages.MessageRecyclerAdapter
import com.example.mapinvader.data.Firebase
import com.example.mapinvader.data.MessageRepository
import com.example.mapinvader.databinding.FragmentChatBinding
import com.example.mapinvader.helpers.DataHolder
import com.example.mapinvader.models.Message

class ChatFragment : Fragment() {
    private lateinit var chatBinding: FragmentChatBinding

            companion

    object {
        fun newInstance(): ChatFragment {
            return ChatFragment()
        }

        lateinit var recyclerView: RecyclerView
        lateinit var adapter: MessageRecyclerAdapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        chatBinding = FragmentChatBinding.inflate(inflater, container, false)
        initializeUI()

        return chatBinding.root
    }

    private fun initializeUI() {
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        layoutManager.stackFromEnd = true
        chatBinding.rvChatMessages.layoutManager = layoutManager
        chatBinding.rvChatMessages.adapter = MessageRecyclerAdapter(MessageRepository.messages)

        chatBinding.btnChatSend.setOnClickListener {
            val messageText = chatBinding.etChatMessage.text.toString().trim()

            if (messageText.isNotEmpty()) {
                chatBinding.etChatMessage.text.clear()
                val sender = DataHolder.getLoggedUser()
                val timeNow = System.currentTimeMillis()

                Firebase.registerNewMessage(Message(messageText, sender, timeNow))
            }
        }
    }

    private fun displayMessages() {
        chatBinding.rvChatMessages.adapter = MessageRecyclerAdapter(MessageRepository.messages)
        recyclerView = chatBinding.rvChatMessages
        adapter = chatBinding.rvChatMessages.adapter as MessageRecyclerAdapter

        Firebase.fetchMessages()
    }

    override fun onPause() {
        super.onPause()
        Firebase.unsubscribeMessageListener()
    }

    override fun onResume() {
        super.onResume()
        displayMessages()
    }
}