package com.example.mapinvader.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mapinvader.MapInvaderApplication
import com.example.mapinvader.R
import com.example.mapinvader.SoundPool
import com.example.mapinvader.adapters.GymAdapter
import com.example.mapinvader.adapters.QuestionAdapter
import com.example.mapinvader.data.Firebase
import com.example.mapinvader.data.QuestionRepository
import com.example.mapinvader.databinding.FragmentMapBinding
import com.example.mapinvader.helpers.DataHolder
import com.example.mapinvader.helpers.getBitmapFromVectorDrawable
import com.example.mapinvader.helpers.hasPermissionCompat
import com.example.mapinvader.ui.MainActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var mapBinding: FragmentMapBinding
    private var firstLoad: Boolean = true
    private lateinit var questionAdapter: QuestionAdapter
    private val sound = SoundPool()

    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val locationRequestCode = 10
    private lateinit var locationManager: LocationManager
    private val locationListener = LocationListener { location -> updateLocationDisplay(location) }

    private var lastKnownLocation: Marker? = null
    private var lastVisitedMarker: Marker? = null
    private var nearbyMarker: Marker? = null
    private val zoomLevel = 15f
    private var googleMap: GoogleMap? = null

    companion object {
        fun newInstance(): MapFragment {
            return MapFragment()
        }

        lateinit var gymAdapter: GymAdapter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mapBinding = FragmentMapBinding.inflate(inflater, container, false)
        initializeUI()

        return mapBinding.root
    }

    private fun updateLocationDisplay(location: Location) {
        lastKnownLocation?.remove()
        val homeLatLng = LatLng(location.latitude, location.longitude)
        firstMapLoad(homeLatLng)

        lastKnownLocation = googleMap!!.addMarker(MarkerOptions()
            .position(homeLatLng).title("Your Location")
            .icon(getBitmapFromVectorDrawable(MapInvaderApplication.ApplicationContext, R.drawable.icon_point_on_me))
        )

        nearbyMarker = gymAdapter.checkForNearbyGyms(homeLatLng)
        if (nearbyMarker != null) {
            if (!gymAdapter.areSameGyms(nearbyMarker!!, lastVisitedMarker)) {
                lastVisitedMarker = nearbyMarker

                if (nearbyMarker!!.title != DataHolder.getLoggedUser())
                    displayQuestionForm()
            }
        }
    }

    private fun displayQuestionForm() {
        val gymId = gymAdapter.getGymIdByPosition(lastVisitedMarker?.position)

        questionAdapter.getRandomQuestion(gymId.toString())
        sound.playSound(R.raw.game_bump)
    }

    private fun firstMapLoad(homeLatLng: LatLng) {
        if (firstLoad) {
            firstLoad = false
            googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(homeLatLng, zoomLevel))

            mapBinding.progressBar.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            locationRequestCode -> {
                if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    trackLocation()
                } else
                    Log.e("permission", "NOT GRANTED!")
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun initializeUI() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)

        locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        sound.loadSounds()
        trackLocation()
    }

    @SuppressLint("MissingPermission")
    private fun startTrackingLocation() {
        val criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE
        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try {
            if (provider != null) {
                locationManager.requestLocationUpdates(provider, minTime, minDistance, locationListener)
            }
        } catch (e: SecurityException) {
            Log.e("TAG", "Tracking location failed - security problem")
        }
    }

    private fun trackLocation() {
        if (hasPermissionCompat(MapInvaderApplication.ApplicationContext, locationPermission)) {
            startTrackingLocation()
        } else {
            requestPermissions(arrayOf(locationPermission), locationRequestCode)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.clear()
        this.googleMap = googleMap
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json))
        gymAdapter = GymAdapter(googleMap)
        questionAdapter = QuestionAdapter(QuestionRepository.questions, context!!)
    }

    override fun onResume() {
        super.onResume()
        MainActivity.viewPager.isUserInputEnabled = false
        Firebase.fetchGyms()
        trackLocation()
    }

    @SuppressLint("MissingPermission")
    override fun onPause() {
        super.onPause()
        MainActivity.viewPager.isUserInputEnabled = true
        locationManager.removeUpdates(locationListener)
        Firebase.unsubscribeGymListener()
        gymAdapter.clearAllMarkers()
    }
}